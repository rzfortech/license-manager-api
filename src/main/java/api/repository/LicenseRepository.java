package api.repository;

import api.entity.Client;
import api.entity.License;
import api.entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface LicenseRepository extends CrudRepository<License, Long> {

    Collection<License> findAllByClientId(Client id);

    @Query(value = "SELECT l FROM License l left join l.clientId c WHERE c.userId = :user ORDER BY l.end desc")
    Collection<License> findAllLicensesBySalesMan(@Param("user") User user);

}